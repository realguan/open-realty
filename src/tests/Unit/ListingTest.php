<?php


namespace Tests\Unit;

use Tests\Support\UnitTester;

require_once dirname(__FILE__).'/../../include/listing.inc.php';
use listing_pages;

class ListingTest extends TestSetup
{
    protected UnitTester $tester;

    protected function _before()
    {
        parent::_before();
    }

    protected function _after()
    {
        parent::_after();
    }
    
    /**
     * Test the pclass_link function
     *
     * @return void
     */
    public function testPclassLink()
    {
        global $config, $api;
        $api = \Mockery::mock();
        $api_result = array();
        //Mock the listing__read api and return pclass ID 2
        $api_result['error'] = false;
        $api_result['listing']['listingsdb_pclass_id'] = 2;
        $api->allows()->load_local_api('listing__read', ['listing_id' => 1, 'fields' => ['listingsdb_pclass_id']])->andReturns($api_result);
        //Mock the pclass__read api and class name "Test Class"
        $api_result = array();
        $api_result['error'] = false;
        $api_result['class_name'] = "Test Class";
        $api->allows()->load_local_api('pclass__read', ['class_id' => 2])->andReturns($api_result);
        //Run the function
        $lp = new listing_pages();
        $link = $lp->pclass_link(1);
        //Set our expected URL base on our mock api calls.
        $expected_url = '<a href="http://web.local/index.php?action=searchresults&amp;pclass[]=2" title="Test Class">Test Class</a>';
        $this->assertEquals($expected_url, $link);
    }
}
